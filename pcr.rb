#!/usr/bin/env ruby

# --- configuration / defaults
#
PORT="/dev/serial/by-id/usb-1a86_USB2.0-Ser_-if00-port0"
SPEED=9600                      # 0 = "don't bother to set" (icom default is 9600)
TIMEOUT=1                       # seconds before giving up on serial read


# --- library
#
def debug s, p='---'
  STDERR.puts "#{p} #{s}"  if $debug
end


def help
  STDERR.puts
  STDERR.puts "usage:       #$0 commands...", ''
  STDERR.puts "port:        #{PORT}"
  STDERR.puts "baudrate:    #{SPEED}"
  STDERR.puts '', 'commands:'
  STDERR.puts 'system:      off  debug  keepalive  model  version  dsp  status  st'
  STDERR.puts 'mode:        am  fm  wfm  cw  lsb  usb'
  STDERR.puts 'on / off:    agc  nb  vsc  att  monitor'
  STDERR.puts '0 to 100:    volume  vol  squelch  sq'
  STDERR.puts 'filter:      3 khz  6 khz  15 khz  50 khz  230 khz'
  STDERR.puts 'frequency:   (in mhz or hz, like 144.8 or 144800000)'
  STDERR.puts
end


require 'io/console'
require 'timeout'
def cmd command                 # send a command, return the reply
  str, out = command.upcase, ''

  begin                         # send
    debug str, "-->"
    $fp.iflush                  # clear input buffer before sending
    Timeout.timeout(TIMEOUT) do
      $fp.puts str + 13.chr     # because commands end with CR/LF
    end
  rescue Timeout::Error
    STDERR.puts "!-- timeout on serial write"
    return ''
  end

  begin                         # receive
    Timeout.timeout(TIMEOUT) do
      loop do
        c = $fp.read(1)         # fetch next byte
        if c
          out += c.chomp
          break  if c == "\n" && out.size > 0
        end
      end
    end
  rescue Timeout::Error
    if out == ''
      STDERR.puts "!-- timeout on serial read"
      exit 2
    end
  end

  debug out, "<--"
  out
end


def try command                 # send a command, returns true if accepted
  r = cmd command

  # Icom radios reply 'G000', not 'OK':
  STDERR.puts "!-- wrong serial port"  if r.chomp == 'OK'
  r.strip.start_with? 'G000'
end

def set command, param=''       # send a command string, possible error message
  command += param
  STDERR.puts "!-- command #{command.inspect} was not accepted"  unless try command
end

def inq command                 # send a command, returns 4-char reply, fail otherwise
  r = cmd command

  if r.size < 4 || (r =~ /[A-Z]/) != 0
    STDERR.puts "!-- invalid return value for command #{command.inspect}: #{r.inspect}"
  end

  r[0..3]
end

def status
  (0..2).map { |i|  (inq "I#{i}?")[-2..-1] }.join "-"
end

def monitor x
  debug "monitor #{x}"
  if x == '00'
    `pactl unload-module module-loopback`.chomp
  elsif x == '01'
    `pactl load-module module-loopback`.chomp
  else
    STDERR.puts "!-- pulseaudio loopback monitor can only be:  on  off"
  end
end


# --- setup
#
if ARGV == [] || ARGV == [ 'help' ] || ARGV == [ '--help' ]
  help
  exit 0
end
$debug = ARGV.first.downcase == 'debug'

unless File.exists? PORT
  STDERR.puts "!-- port is not available: #{PORT}"
  exit 1
end

if SPEED > 0
  params = 'sane raw pass8 -crtscts -clocal'
  params = 'raw pass8 -crtscts'
  stty = `stty --file=#{PORT} ispeed #{SPEED} ospeed #{SPEED} #{params} < #{PORT}; stty --file=#{PORT} -a`
  expe = "speed #{SPEED}"
  unless stty.start_with?(expe)
    STDERR.puts "!-- stty error: #{stty}"
    exit 1
  end
  sleep 0.1                     # let the serial driver digest it
end

$keep = false
$fp = File.open(PORT, 'w+')     # crash if can't open
sleep 0.1                       # give some time before iflush'ing

3.times do                      # try to turn on a few times
  cmd 'H101'
  break  if cmd('H1?') == 'H101'
end


# --- commands and modes
#
$mode='05'                      # default: FM (narrow)
$filt='02'                      # default: 15 kHz

CMD={ 'help'    =>    -> { help             },
      '--help'  =>    -> { help             },
      'model'   =>    -> { inq 'G4?'        },  # G412 = Icom IC-PCR100; G410 = Icom IC-PCR1000
      'version' =>    -> { inq 'GE?'        },  # GE09 = USA, GE02 EUR/AUS
      'dsp'     =>    -> { inq 'GD?'        },  # GD00 = not present (PCR100); GD01 = present (PCR1000 etc)
      'keepalive' =>  -> { $keep=true;  nil },
      'status'  =>    -> { status           },  # squelch status - signal strenght - signal centering
      'st'      =>    -> { status           },  # (04=closed,07=open) - (00 to ff) - (00=low, 80=centered, ff=high)
      'off'     =>    -> { cmd 'H100'       },
      'debug'   =>    -> { $debug=true; nil },
      'lsb'     =>    -> { $mode='00';  nil },
      'usb'     =>    -> { $mode='01';  nil },
      'am'      =>    -> { $mode='02';  nil },
      'cw'      =>    -> { $mode='03';  nil },
      'fm'      =>    -> { $mode='05';  nil },
      'wfm'     =>    -> { $mode='06';  nil },
      'radio'   =>    -> { set('J45', '01');    # radio broadcast setup:
	                   set('J41', '30');    # attenuation, squelched,
			   $mode='06';          # wide-fm mode,
			   $filt='04';  nil },  # 230 kHz filter
}

SET={ 'monitor' => ->(n) { monitor n        },
      'sq'      => ->(n) { set('J41', n)    },  # shortcut for squelch
      'vol'     => ->(n) { set('J40', n)    },  # shortcut for volume
      'squelch' => ->(n) { set('J41', n)    },
      'volume'  => ->(n) { set('J40', n)    },
      'agc'     => ->(n) { set('J45', n)    },  # automatic gain control
      'att'     => ->(n) { set('J47', n)    },  # attenuation control
      'nb'      => ->(n) { set('J46', n)    },  # noise blanker (not on pcr100)
      'vsc'     => ->(n) { set('J50', n)    },  # not supported on pcr100
}

FILT={ 3 => '00', 6 => '01', 15 => '02', 50 => '03', 230 => '04' }


# --- main

if ARGV == [] || ARGV == [ 'help' ] || ARGV == [ '--help' ]
  help
  exit 0
end

while a = ARGV.shift            # fetch next command
  b = a.downcase
  c = CMD[b]                    # stand-alone command?
  if c
    debug a
    r = c.call
    puts r.inspect  if r
    next
  end

  c = SET[b]                    # command requiring a parameter?
  if c
    b = ARGV.shift
    n = if b
          d = b.downcase
          d = '1'  if d == 'on'
          d = '0'  if d == 'off'
          n = d.to_i
        end

    if n == nil ||
        (n == 0 && b != '0' && "#{b}".downcase != 'off') ||
        n < 0 || n > 100
      STDERR.puts "!-- expected a number parameter: #{a.inspect} #{b.inspect}"
      exit 3
    end

    # convert the numeric parameter to 5..255 if above 1, then execute
    # - note: IC-PCR100 generally goes silent if requested volume > 70*2.55
    #
    debug "#{a} #{n}"
    n = (n * 2.55).floor  if n > 1
    r = c.call("%02x" % [ n ])
    puts r.inspect  if r
    next
  end

  # do we have a numeric parameter?
  #
  n = if a =~ /^[0-9]+\.[0-9]*$/
        (a.to_f * 1000000.0).floor
      elsif a =~ /^[0-9]+$/
        a.to_i
      end

  unless n
    STDERR.puts "!-- syntax error: #{a.inspect}"
    exit 3
  end

  if "#{ARGV.first}".downcase == 'khz'
    f = FILT[n]
    if f
      debug "khz mode #{f}"
      $filt = f
    else
      STDERR.print "!-- invalid khz filter; allowed values are:"
      FILT.each_key { |k|  STDERR.print " #{k}" }
      STDERR.puts
    end
    ARGV.shift  if ARGV.size > 0
    next
  end

  if n < 10000 || n > 1300000000
    STDERR.puts "!-- ignoring wrong frequency: #{n.inspect}"
    next
  end

  r = set('K0%010d%s%s00' % [ n, $mode, $filt ])
  puts r.inspect  if r
end

begin
  while $keep
    sleep 5
    if cmd('H1?') != 'H101'
      STDERR.puts "!-- radio is off"
      exit 2
    end
    sleep 25
  end
rescue Interrupt => _
  STDERR.puts '','!-- exiting...'
end

# ---
