## Description

Command-line utility to set up Icom IC-PCR100/PCR1000 receivers.

Only intended for basic stuff: turn on the radio, set parameters, tune frequency, maybe turn off the radio.

Note: except if only invoked with *help,* it will turn on the radio (its LED will light up). The radio will turn itself to standby by default in some 30 seconds, or when using the *off* command. The *keepalive* command will keep it on.

## Setup and test

- *ruby* and *stty* executables should be already installed on the system
- edit the *pcr.rb* script at the beginning to set the serial *PORT* 
- connect the serial port cable to the PCR100/PCR1000
- plug the PCR100/1000 power supply
- test using the command: *./pcr.rb model version dsp*
- tune some channel: *./pcr.rb fm 15 khz 144.8 volume 41 squelch 27 agc on*
- turn off the radio: *./pcr.rb off*

Note: default factory setting for serial port is 9600 baud; Icom manual suggests 38400 to use with polling mode updates; my script doesn't use *Gx00* polling.

## Monitoring 

PulseAudio sound system (installed by default on Ubuntu Linux) allows turning on/off the mic monitoring. The script may invoke *pactl* for you:

- connect PCR100/1000 audio output to PC's *mic* input (2x 3.5mm male jack)
- connect headphones to PC's *ear* output
- tune some broadcast: *./pcr.rb wfm 230 khz 103.9 volume 45 agc off monitor on*
- record radio input while listening (use ctrl-C to stop): *parec --file-format=wav > myfile.wav*
- turn off radio and PulseAudio mic monitoring: *./pcr.rb off monitor off*

## Command-line reference

System commands:

- *off* - turns off the PCR100/1000
- *debug* - enables debug mode to watch serial strings
- *status* or *st* - reports current squelch status (40=closed), signal strenght (00 to ff) and signal centering (80=centered, 00=low, ff=high)
- *am fm wfm cw lsb usb* - mode selection for next frequency tune command; note that PCR100 doesn't support side-band (LSB and USB) modes
- *keepalive* - do not exit while the radio is still on (check every 30 seconds)

Volume/squelch settings:

- *volume* or *vol* - sets volume (0 to 100)
- *squelch* or *sq* - sets squelch (0 to 100)

Switches (expecting *on* or *off* as a parameter):

- *agc* - automatic gain control
- *nb* - noise blanker (not available on PCR100)
- *att* - attenuator
- *vsc* - voice squelch control (not available on PCR100)
- *monitor* - set PulseAudio mic-monitoring

IF filter selection (will be set on next frequency tune command):

- *3 khz* - sets 3 (actually 2.8, usually for CW) kHz
- *6 khz* - sets 6 kHz (usually for AM, selectable for FM)
- *15 khz* - sets 15 kHz (usually for FM, selectable for AM)
- *50 khz* - sets 50 kHz (selectable for AM, FM, Wide-FM modes)
- *230 khz* - sets 230 kHz (usually Wide-FM mode)

Frequency tune: either in MHz or Hz. For example, these two are equivalent:

- *144800000*
- *144.8*

Note: not all combinations are possible.

## Command-line examples

Say you installed *pcr.rb* in your *$HOME/bin/* directory. You could use some aliases in *$HOME/.bash_aliases* like:

    alias radiooff='~/bin/pcr.rb off'
    alias radiotest='~/bin/pcr.rb debug model version dsp 230 khz wfm 103.9 sq 0 vol 41 agc off att on st'
    alias tuneaprs='~/bin/pcr.rb fm 15 khz 144.8 volume 41 squelch 27 agc on'
    alias raiclassica='~/bin/pcr.rb 230 khz wfm 103.9 vol 41 agc off att on'
    alias approach='~/bin/pcr.rb 6 khz am sq 27 vol 45 124.35 agc on'
